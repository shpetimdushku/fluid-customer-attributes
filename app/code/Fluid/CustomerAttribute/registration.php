<?php
/**
 * Module configuration
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Fluid_CustomerAttribute',
    __DIR__
);
