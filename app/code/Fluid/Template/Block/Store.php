<?php

namespace Fluid\Template\Block;

class Store extends \Magento\Framework\View\Element\Template
{

    protected $_store;
    protected $_cmspage;
    protected $_cmshelper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Locale\Resolver $store,
        \Magento\Cms\Model\Page $cmspage,
        \Magento\Cms\Helper\Page $cmshelper,
        array $data = []
    )
    {
        $this->_store = $store;
        $this->_cmspage = $cmspage;
        $this->_cmshelper = $cmshelper;
        parent::__construct($context, $data);

    }

    public function getLanguageCode()
    {
        return $this->_store->getLocale();
    }

    public function getCmsID()
    {
        return $this->_cmspage->getId();
    }

    public function getCmsStoresID()
    {
        $stores = $this->_cmspage->getStores();
        return $stores[0];
    }

    public function getCmsUrl($cmsid)
    {
        return $this->_cmshelper->getPageUrl($cmsid);
    }
}